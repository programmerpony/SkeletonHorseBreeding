package com.programmerpony.skeletonhorsebreeding;

import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.spigotmc.event.entity.EntityMountEvent;

import java.util.List;

public class SkeletonHorseBreedListener implements Listener {

    @EventHandler
    public void onEntityClick(PlayerInteractEntityEvent event) {

        Player p = event.getPlayer();
        if (event.getHand() != EquipmentSlot.HAND) return; // Ignore off hand

        if (event.getRightClicked() instanceof SkeletonHorse) {
            SkeletonHorse skeletonHorse = (SkeletonHorse) event.getRightClicked();
            if (skeletonHorse.getLoveModeTicks() == 0 && skeletonHorse.isAdult()) {
                ItemStack itemInHand = new ItemStack(p.getInventory().getItemInMainHand());
                if (itemInHand.getType() == Material.BONE_MEAL) {
                    if (p.getGameMode() != GameMode.CREATIVE) p.getInventory().setItemInMainHand(new ItemStack(Material.BONE_MEAL,itemInHand.getAmount()-1));
                    skeletonHorse.getWorld().spawnParticle(Particle.DAMAGE_INDICATOR, skeletonHorse.getLocation().getX(),skeletonHorse.getLocation().getY()+1,skeletonHorse.getLocation().getZ(), 10);
                    skeletonHorse.setLoveModeTicks(400);
                    List<Entity> ents = skeletonHorse.getNearbyEntities(8,1,8);
                    for (Entity ent : ents) if (ent instanceof SkeletonHorse) {
                        SkeletonHorse mate = (SkeletonHorse) ent;
                        if (mate.getLoveModeTicks() > 0 && mate.isAdult()) {
                            skeletonHorse.getWorld().spawnParticle(Particle.DAMAGE_INDICATOR, mate.getLocation().getX(),mate.getLocation().getY()+1,mate.getLocation().getZ(), 10);
                            Location birthLocation = new Location(skeletonHorse.getWorld(),(skeletonHorse.getLocation().getX()+mate.getLocation().getX())/2,skeletonHorse.getLocation().getY(),(skeletonHorse.getLocation().getZ()+mate.getLocation().getZ())/2);
                            skeletonHorse.getWorld().strikeLightningEffect(birthLocation);
                            SkeletonHorse newborn = (SkeletonHorse) skeletonHorse.getWorld().spawnEntity(birthLocation,EntityType.SKELETON_HORSE);
                            newborn.setBaby();
                            newborn.setTamed(true);
                            skeletonHorse.setLoveModeTicks(0);
                            mate.setLoveModeTicks(0);
                            break;
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void doNotMountHorse(EntityMountEvent event) {
        if (event.getMount() instanceof SkeletonHorse && event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            ItemStack itemInHand = new ItemStack(p.getInventory().getItemInMainHand());
            if (itemInHand.getType() == Material.BONE_MEAL) event.setCancelled(true);
        }
    }
}
