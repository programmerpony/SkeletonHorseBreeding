# SkeletonHorseBreeding
 Breed Skeleton Horses with bone meal!

![](https://www.spigotmc.org/data/resource_icons/85/85627.jpg)

 Download via
 [Spigot](https://www.spigotmc.org/resources/skeletonhorsebreeding.85627/) |
 [Bukkit](https://dev.bukkit.org/projects/skeletonhorsebreeding) |
 [CurseForge](https://www.curseforge.com/minecraft/bukkit-plugins/skeletonhorsebreeding) |
 [Modrinth](https://modrinth.com/plugin/skeletonhorsebreeding)
